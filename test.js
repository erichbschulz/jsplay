function readTextFile(file) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function () {
        if(rawFile.readyState === 4) {
            if(rawFile.status === 200 || rawFile.status == 0) {
                var allText = rawFile.responseText;
                initPasswords(allText);
            }
        }
    }
    rawFile.send(null);
}

function initPasswords(data) {
  document.getElementById("demo").innerHTML="data: "+data;
}

function go() {
  readTextFile('data.json');
}
